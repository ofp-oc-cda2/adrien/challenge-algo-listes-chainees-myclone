# Les structures en chaîne

## Chaînon
Chaque élément de la chaine contient une valeur ainsi qu'un lien vers son suivant (ou `null` s'il n'y en a pas)

```ts
export interface Link<T> {  
  value: T  
  next: Link<T> | null  
}
```

## Structure
Représentation des données pour stocker les valeurs 1, 2, 3 et 4

```mermaid
graph LR
H((head))-->1
1-->2
2-->3
3-->4
```


## Parcours 

Pour parcourir la chaîne, se déplacer d'élément en élément tant qu'il y a un suivant (ou autre condition)

```mermaid
graph LR
    S((start))
    head[set current to head]
    process[process current value]
    test{current has next ?}
    next[set current to next]
    E((end))
    
    S --> head
    head --> process
    process --> test
    next --> process
    test -- no --> E
    test -- yes --> next
```


## Insertion

Lors d'une insertion à une position donnée, lier le nouvel élément à son suivant, puis lier l'élément courant vers le nouveau

Algorithme :
```mermaid
graph LR
    S((start))
    move[move to position]
    updatenew[link new to next]
    updatecurrent[link current to new]
    E((end))

    S --> move
    move --> updatenew
    updatenew --> updatecurrent
    updatecurrent --> E
```

Structure pour insertion de `4` entre `3` et `5` :
```mermaid
graph LR
H((head))-->1
    1-->2
    2-->3
    3-->4
    4-->5
    3-->5
    style 4 stroke:#6f6,stroke-width:2px
    linkStyle 3 stroke:#6f6
    linkStyle 4 stroke:#6f6
    linkStyle 5 stroke:#f66
```

## Suppression

Pour la suppression à une position donnée, lier l'élément courant à l'élément suivant celui à supprimer

Algorithme :
```mermaid
graph LR
    S((start))
    move[move to position]
    updatecurrent[link current to next.next]
    E((end))

    S --> move
    move --> updatecurrent
    updatecurrent --> E
```

Structure pour suppression de `4` entre `3` et `5` :
```mermaid
graph LR
H((head))-->1
    1-->2
    2-->3
    3-->4
    4-->5
    3-->5
    style 4 stroke:#f66,stroke-width:2px
    linkStyle 3 stroke:#f66
    linkStyle 4 stroke:#f66
    linkStyle 5 stroke:#6f6
```
