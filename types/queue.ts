import { List } from './list'


export interface Queue<T> extends List<T> {
  pushEnd(value: T): void
  popEnd(): T

  forEachR(callback: ForEachCallback<T>): void
  mapR(callback: MapCallback<T>): T[]
  reduceR<A>(acc: A, callback: ReduceCallback<T, A>): A
}
