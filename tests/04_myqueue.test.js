const { StackTests } = require('./stack.js')
const { ListTests } = require('./list.js')
const { QueueTests } = require('./queue.js')

const { MyQueue } = require('../src/myqueue')

describe('MyQueue', () => {
  StackTests(MyQueue) // Execute stack tests
  ListTests(MyQueue) // Execute list tests
  QueueTests(MyQueue) // Execute queue tests
})
