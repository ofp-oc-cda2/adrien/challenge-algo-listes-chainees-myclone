const { StackTests } = require('./stack.js')
const { ListTests } = require('./list.js')
const { QueueTests } = require('./queue.js')

const { MyFirstQueue } = require('../src/myfirstqueue')

describe('MyFirstQueue', () => {
  StackTests(MyFirstQueue) // Execute stack tests
  ListTests(MyFirstQueue) // Execute list tests
  QueueTests(MyFirstQueue) // Execute queue tests
})
