const { StackTests } = require('./stack.js')
const { ListTests } = require('./list.js')

const { MyList } = require('../src/mylist')

describe('MyList', () => {
  StackTests(MyList) // Execute stack tests
  ListTests(MyList) // Execute list tests
})
